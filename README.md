# FSCI Resources

## Docker image for Debian packaging

This docker image consists of basic tools required to package Ruby as well as
NodeJS libraries for Debian. Usage instructions follow

1. Install `docker.io` package to install Docker. In Debian, it is available
   from jessie-backports.

1. Run the following command to get the docker image
  ```
    $ docker pull registry.gitlab.com/fsci/resources/debian-dev:latest
  ```

1. Start a container with the image
  ```
    $ docker run --name debian-dev -it registry.gitlab.com/fsci/resources/debian-dev:latest bash
  ```

1. While you are inside the image, do an update/upgrade to make sure you get the
   latest packages. Default sudo password for the user `developer` is
   `developer` itself.
  ```
    $ sudo apt-get update && sudo apt-get dist-upgrade
  ```

1. Once your work is over, exit the container by pressing Ctrl-D or the command
   `$ exit`

1. In future, start and attach to the container using following commands
  ```
    $ docker start debian-dev
    $ docker attach debian-dev
  ```

#### For packaging workshops

For packaging workshops, where every participant pulling the image from upstream
registry can use quite some bandwidth, the image can be exported as a tarball
and distributed. 

1. If you have the docker image on one of the machines, run the following
   command to export it as a tarball.
  ```
    $ docker save registry.gitlab.com/fsci/resources/debian-dev:latest > debian-dev.tar
    $ gzip debian-dev.tar
  ```

1. Now distribute the `debian-dev.tar.gz` file to the participants

1. On the participant's machine, load the image using the command
  ```
    $ docker load < debian-dev.tar.gz
  ```
